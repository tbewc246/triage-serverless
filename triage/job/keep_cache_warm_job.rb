# frozen_string_literal: true

require 'sucker_punch'

module Triage
  class KeepCacheWarmJob
    include SuckerPunch::Job
    max_jobs 1

    def perform
      puts "Keeping the cache warm..."
      Triage.gitlab_org_group_member_usernames(fresh: true)
      Triage.gitlab_com_group_member_usernames(fresh: true)
      Triage.gitlab_core_team_community_members_group_member_usernames(fresh: true)

      self.class.perform_in(Triage::GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION - 60)
    end
  end
end
