# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/customer_label'
require_relative '../processor/customer_contribution_notifier'
require_relative '../processor/deprecated_label'
require_relative '../processor/hackathon_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/merge_request_help'
require_relative '../processor/reactive_labeler'
require_relative '../processor/thank_community_contribution'
require_relative '../processor/type_label'

require 'rack'
require 'digest/sha2'

module Triage
  class Handler
    DEFAULT_TRIAGERS = [
      AvailabilityPriority,
      CustomerLabel,
      TypeLabel,
      BackstageLabel,
      DeprecatedLabel,
      HackathonLabel,
      ThankCommunityContribution,
      DocCommunityContribution,
      MergeRequestHelp,
      ReactiveLabeler,
      CustomerContributionNotifier
    ].freeze

    def initialize(env, event_hash, triagers: DEFAULT_TRIAGERS)
      raise ArgumentError if webhook_token_empty?

      @env = env
      @event_hash = event_hash
      @triagers = triagers
    end

    def authenticated?
      secure_compare(http_gitlab_token, webhook_token)
    end

    def process
      triagers.map do |triager|
        triager.triage(event)
      end.compact
    end

    private

    attr_reader :env, :event_hash, :triagers

    def event
      @event ||= Event.build(event_hash)
    end

    def webhook_token_empty?
      webhook_token.match?(/\A\s*\z/)
    end

    def http_gitlab_token
      @http_gitlab_token ||= env['HTTP_X_GITLAB_TOKEN'].to_s
    end

    def webhook_token
      @webhook_token ||= ENV['GITLAB_WEBHOOK_TOKEN'].to_s
    end

    def secure_compare(a, b)
      Rack::Utils.secure_compare(
        ::Digest::SHA256.hexdigest(a),
        ::Digest::SHA256.hexdigest(b)) &&
        a == b
    end
  end
end
