# frozen_string_literal: true

require 'mini_cache'
require 'gitlab'
require 'sucker_punch'

require_relative 'triage/error'
require_relative 'triage/event'
require_relative 'triage/handler'
require_relative 'triage/sentry'

module Triage
  PRODUCTION_API_ENDPOINT = 'https://gitlab.com/api/v4'
  GITLAB_ORG_GROUP = 'gitlab-org'
  GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour
  GITLAB_COM_GROUP = 'gitlab-com'
  GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour
  GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP = 'gitlab-org/gitlab-core-team/community-members'
  GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION = 3600 # 1 hour
  GROUP_MEMBER_ATTRS_CACHEABLE = %i[id username].freeze

  def self.run!(context:, event:)
    handler = Handler.new(context, event)

    puts "Running in dry mode" if dry_run?

    if handler.authenticated?
      messages = handler.process

      puts "Incoming webhook event: #{event.inspect}, messages: #{messages.inspect}"

      { status: :ok, messages: messages }
    else
      puts "Unauthenticated request!"

      { status: :unauthenticated }
    end
  rescue Triage::ClientError => error # Don't track client errors
    error_response(error)
  rescue => error
    Raven.tags_context(object_kind: event['object_kind'])
    Raven.extra_context(event: event)
    Raven.capture_exception(error)

    puts "Exception with event: #{event.inspect}, error: #{error.class}: #{error.message}"
    error_response(error)
  end

  def self.error_response(error)
    { status: :error, error: error.class, message: error.message }
  end

  def self.dry_run?
    !ENV['DRY_RUN'].nil?
  end

  def self.cache
    @cache ||= MiniCache::Store.new
  end

  def self.api_client
    cache.get_or_set(:api_client) do
      Gitlab.client(endpoint: PRODUCTION_API_ENDPOINT, private_token: ENV['GITLAB_API_TOKEN'].to_s)
    end
  end

  def self.gitlab_org_group_member_ids(fresh: false)
    group_member_ids(GITLAB_ORG_GROUP, fresh: fresh, expires_in: GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_com_group_member_ids(fresh: false)
    group_member_ids(GITLAB_COM_GROUP, fresh: fresh, expires_in: GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_core_team_community_members_group_member_ids(fresh: false)
    group_member_ids(GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP, fresh: fresh, expires_in: GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_org_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_ORG_GROUP, fresh: fresh, expires_in: GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_com_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_COM_GROUP, fresh: fresh, expires_in: GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  def self.gitlab_core_team_community_members_group_member_usernames(fresh: false)
    group_member_usernames(GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP, fresh: fresh, expires_in: GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION)
  end

  class << self
    GROUP_MEMBER_ATTRS_CACHEABLE.each do |attr|
      define_method "group_member_#{attr}s" do |group, fresh:, expires_in:|
        group_member_attrs(group, attr, fresh: fresh, expires_in: expires_in)
      end
      private :"group_member_#{attr}s"
    end
  end

  def self.group_member_attrs(group, attr, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set
    # Make sure we have the up-to-date source data
    cached_group_members = group_members(group, fresh: fresh, expires_in: expires_in)

    cache.public_send(cache_method, "#{group}_group_member_#{attr}s".to_sym) do
      member_attrs = cached_group_members.map(&attr)
      # Don't set an expiration time here as these caches are cleared in `.group_members`.
      MiniCache::Data.new(member_attrs)
    end
  end
  private_class_method :group_member_attrs

  def self.group_members(group, fresh:, expires_in:)
    cache_method = fresh ? :set : :get_or_set

    cache.public_send(cache_method, "#{group}_group_members".to_sym) do
      MiniCache::Data.new(fresh_group_members(group), expires_in: expires_in).tap do
        group_member_attrs_cache_keys(group).each { |cache_key| cache.unset(cache_key) }
      end
    end
  end
  private_class_method :group_members

  def self.group_member_attrs_cache_keys(group)
    GROUP_MEMBER_ATTRS_CACHEABLE.map { |attr| "#{group}_group_member_#{attr}s".to_sym }
  end

  def self.fresh_group_members(group)
    api_client
      .group_members(group, per_page: 100)
      .auto_paginate
  end
  private_class_method :fresh_group_members
end
