# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/documentation_code_owner'

module Triage
  class DocCommunityContribution < Triager
    DOC_FILE_REGEX = %r{\Adoc/}
    DOCUMENTATION_LABEL = 'documentation'
    TECHNICAL_WRITING_LABEL = 'Technical Writing'
    TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'

    def applicable?
      event.from_gitlab_org? &&
        event.merge_request? &&
        event.wider_community_author? &&
        !event.wip? &&
        !merge_request_labelled_technical_writing? &&
        merge_request_changes_doc?
    end

    def process
      post_documentation_label_comment
    end

    private

    def merge_request_changes_doc?
      merge_request_changes.any? do |change|
        doc_change?(change)
      end
    end

    def merge_request_labelled_technical_writing?
      event.label_names.any? do |label|
        label == TECHNICAL_WRITING_TRIAGED_LABEL ||
          label == TECHNICAL_WRITING_LABEL ||
          label.start_with?('tw::')
      end
    end

    def doc_change?(change)
      %w[old_path new_path].any? do |path|
        change[path].match(DOC_FILE_REGEX)
      end
    end

    def merge_request_changes
      Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def post_documentation_label_comment
      comment = <<~MARKDOWN.chomp
        #{message if approver_usernames.any?}
        /label ~"#{DOCUMENTATION_LABEL}" ~"#{TECHNICAL_WRITING_TRIAGED_LABEL}"
      MARKDOWN

      add_comment(comment.strip)
    end

    def message
      <<~MESSAGE
        Hi #{approver_usernames.join(' ')}, please review this ~"#{DOCUMENTATION_LABEL}" Merge Request.
      MESSAGE
    end

    def approver_usernames
      approvers = Triage::DocumentationCodeOwner.new(project_id, merge_request_iid).approvers
      approvers.map { |username| "@#{username}"}
    end
  end
end
