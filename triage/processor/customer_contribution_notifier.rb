# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/org_by_username_locator'

require 'slack-messenger'

module Triage
  class CustomerContributionNotifier < Triager
    CUSTOMER_PORTAL_URL = 'https://gitlab.my.salesforce.com/'
    GITLAB_INSTANCE_URL = 'https://gitlab.com/'
    GITLAB_BOT = '@gitlab-bot'
    SLACK_CHANNEL = '#mrarr-wins'
    SLACK_ICON = ':robot_face:'
    DEFAULT_SLACK_OPTIONS = {
      channel: SLACK_CHANNEL,
      username: GITLAB_BOT,
      icon_emoji: SLACK_ICON
    }.freeze
    CUSTOMER_CONTRIBUTION_MESSAGE_TEMPLATE = <<~MESSAGE
      > Organization: #{CUSTOMER_PORTAL_URL}%<org_name>s
      > Contribution Type: %<contribution_type>s
      > MR Link: %<mr_url>s
    MESSAGE

    CONTRIBUTION_TYPE_LABELS = [
      'feature',
      'bug',
      'documentation',
      'tooling'
    ].freeze

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
      (event.merge_request? && event.merge_event?) &&
      community_contribution? &&
      contribution_from_customer_author?
    end

    def process
      notify_customer_contribution_channel if applicable?
    end

    private

    attr_reader :messenger

    def community_contribution?
      event.wider_community_author?
    end

    def contribution_from_customer_author?
      !!org_name
    end

    def notify_customer_contribution_channel
      messenger.ping(formatted_message)
    end

    def formatted_message
      @formatted_message ||= format(CUSTOMER_CONTRIBUTION_MESSAGE_TEMPLATE,
        org_name: org_name,
        contribution_type: contribution_type,
        mr_url: mr_url)
    end

    def org_name
      @org_name ||= OrgByUsernameLocator.locate_org(event.author_id)
    end

    def contribution_type
      @contribution_type ||= event.label_names.find { |label_name| CONTRIBUTION_TYPE_LABELS.include?(label_name) }
    end

    def mr_url
      event.url
    end

    def slack_messenger
      Slack::Messenger.new(ENV['SLACK_WEBHOOK_URL'], DEFAULT_SLACK_OPTIONS)
    end
  end
end
