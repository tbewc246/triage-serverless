# frozen_string_literal: true

require 'json'
require 'sucker_punch'
require_relative 'triage/triage'
require_relative 'triage/job/keep_cache_warm_job'

use Rack::CommonLogger, $stdout
use Rack::ContentType, 'application/json'

SuckerPunch.logger = Logger.new($stdout)
SuckerPunch.exception_handler = -> (error, klass, args) do
  Raven.tags_context(process: :sucker_punch)
  Raven.extra_context(job_class: klass, job_args: args)
  Raven.capture_exception(error)

  puts "SuckerPunch error for job #{klass.inspect} with args #{args}, error: #{error.class}: #{error.message}"
end

# Warm the cache immediately
Triage::KeepCacheWarmJob.perform_async

run lambda { |env|
  request = Rack::Request.new(env)
  event = JSON.parse("[#{request.body.read}]").first || {}

  response = Triage.run!(context: env, event: event)

  [200, {}, [JSON.dump(response)]]
}
