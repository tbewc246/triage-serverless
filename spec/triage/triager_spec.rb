# frozen_string_literal: true
require 'spec_helper'

require_relative '../../triage/triage/triager'

describe Triage::Triager do
  describe '#triage' do
    subject { described_class.new(double(:event)) }

    context 'if event is applicable' do
      it 'calls hook methods' do
        expect(subject).to receive(:applicable?).ordered.and_return(true)
        expect(subject).to receive(:before_process).ordered
        expect(subject).to receive(:process).ordered
        expect(subject).to receive(:after_process).ordered

        subject.triage
      end
    end

    context 'if event is not applicable' do
      it 'does not call hooks' do
        expect(subject).to receive(:applicable?).and_return(false)
        expect(subject).not_to receive(:before_process)
        expect(subject).not_to receive(:process)
        expect(subject).not_to receive(:after_process)

        subject.triage
      end
    end
  end
end
