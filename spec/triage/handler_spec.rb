# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  subject { described_class.new(context, event, triagers: [triager1, triager2]) }

  let(:context) { { 'HTTP_X_GITLAB_TOKEN' => input_token } }
  let(:event) { { 'object_kind' => 'issue' } }
  let(:input_token) { 'input_token' }
  let(:triager1) { class_double('Triage::Triager', triage: true) }
  let(:triager2) { class_double('Triage::Triager', triage: nil) }

  describe 'DEFAULT_TRIAGERS' do
    it 'includes all triager implementations' do
      expected = [
        Triage::AvailabilityPriority,
        Triage::CustomerLabel,
        Triage::TypeLabel,
        Triage::BackstageLabel,
        Triage::DeprecatedLabel,
        Triage::HackathonLabel,
        Triage::ThankCommunityContribution,
        Triage::DocCommunityContribution,
        Triage::MergeRequestHelp,
        Triage::ReactiveLabeler,
        Triage::CustomerContributionNotifier
      ]

      expect(described_class::DEFAULT_TRIAGERS).to eq(expected)
    end
  end

  describe '#initialize' do
    context 'when webhook_token is empty' do
      before do
        stub_env('GITLAB_WEBHOOK_TOKEN' => '')
      end

      it 'raises ArgumentError' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#authenticated?' do
    context 'when webhook_token has something' do
      before do
        stub_env('GITLAB_WEBHOOK_TOKEN' => actual_token)

        expect(subject).to receive(:secure_compare).and_call_original
      end

      context 'when actual token is the same' do
        let(:actual_token) { input_token }

        it 'returns true' do
          expect(subject).to be_authenticated
        end
      end

      context 'when actual token is different' do
        let(:actual_token) { 'fake' }

        it 'returns false' do
          expect(subject).not_to be_authenticated
        end
      end
    end
  end

  describe '#process' do
    it 'executes all triagers' do
      subject.process

      expect(triager1).to have_received(:triage).once
      expect(triager2).to have_received(:triage).once
    end

    it 'removes nil result from triager' do
      expect(subject.process.all?).to be_truthy
    end
  end
end
