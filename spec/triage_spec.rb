require 'spec_helper'

require_relative '../triage/triage'

RSpec.describe Triage do
  describe '.run!' do
    let!(:handler) { Triage::Handler.new(context, event) }
    let(:context) { {'HTTP_X_GITLAB_TOKEN' => webhook_token} }
    let(:object_kind) { 'issue' }
    let(:event) { { 'object_kind' => object_kind } }

    before do
      allow(Triage).to receive(:puts)
      allow(Triage::Handler).to receive(:new).and_return(handler)
      allow(handler).to receive(:authenticated?).and_return(authenticated)
    end

    context 'when not authenticated' do
      let(:authenticated) { false }

      it 'returns { status: :unauthenticated }' do
        result = Triage.run!(context: context, event: event)

        expect(result).to eq(status: :unauthenticated)
      end

      context 'with empty event' do
        let(:event) { {} }

        it 'returns { status: :unauthenticated }' do
          result = Triage.run!(context: context, event: event)

          expect(result).to eq(status: :unauthenticated)
        end
      end
    end

    context 'when authenticated' do
      let(:authenticated) { true }

      it 'calls handler.process and returns status and message' do
        messages = %w[message1 message2]
        expect(handler).to receive(:process).and_return(messages)

        result = described_class.run!(context: context, event: event)

        expect(result).to eq(status: :ok, messages: messages)
      end

      context 'with empty event' do
        let(:event) { {} }

        it 'returns an error with Triage::Event::ObjectKindNotProvidedError' do
          result = described_class.run!(context: context, event: event)

          expect(result).to include(status: :error, error: Triage::Event::ObjectKindNotProvidedError, message: a_kind_of(String))
        end
      end

      context 'with runtime error' do
        let(:error) { StandardError.new('runtime error') }

        before do
          allow(handler).to receive(:process).and_raise(error)
        end

        it 'captures exception into Sentry' do
          expect(Raven).to receive(:capture_exception).with(error)

          Triage.run!(context: context, event: event)
        end

        it 'tags Sentry error with event type' do
          expect(Raven).to receive(:tags_context).with(object_kind: object_kind)

          Triage.run!(context: context, event: event)
        end

        it 'adds event into Sentry as extra context' do
          expect(Raven).to receive(:extra_context).with(event: event)

          Triage.run!(context: context, event: event)
        end
      end

      shared_examples 'cached group member attributes' do |method_name, attr, group, expires_in|
        let(:method) { ->(**args) { described_class.public_send(method_name, **args) } }
        let(:escaped_group) { CGI.escape(group) }
        let(:members_response_1) { [{ id: 1, username: 'root' }] }
        let(:members_response_2) { [{ id: 2, username: 'another-user' }] }
        let(:members_attrs_1) { members_response_1.map { |h| h[attr] } }
        let(:members_attrs_2) { members_response_2.map { |h| h[attr] } }

        it 'caches the API response', :clean_cache do
          expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_1) do
            # Populate the cache
            expect(method.call).to eq(members_attrs_1)
          end

          # Next call is retrieved from cache
          expect(method.call).to eq(members_attrs_1)

          # Travel 1 second after the cache expires
          Timecop.travel(Time.now + expires_in + 1)

          expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_2) do
            # Ensure the response is up-to-date
            expect(method.call).to eq(members_attrs_2)
          end

          # Next call is retrieved from cache
          expect(method.call).to eq(members_attrs_2)
        end

        context 'when `fresh: true` is passed', :clean_cache do
          it 'calls the API and saves the response in the cache' do
            expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_1) do
              # Populate the cache
              expect(method.call).to eq(members_attrs_1)
            end

            expect_api_request(path: "/groups/#{escaped_group}/members", query: { per_page: 100 }, response_body: members_response_2) do
              # Ensure the response is up-to-date
              expect(method.call(fresh: true)).to eq(members_attrs_2)
            end

            # Next call without `fresh: true` is retrieved from cache
            expect(method.call).to eq(members_attrs_2)
          end
        end
      end

      describe '.gitlab_org_group_member_ids' do
        it_behaves_like 'cached group member attributes', :gitlab_org_group_member_ids, :id, described_class::GITLAB_ORG_GROUP, described_class::GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION
      end

      describe '.gitlab_com_group_member_ids' do
        it_behaves_like 'cached group member attributes', :gitlab_com_group_member_ids, :id, described_class::GITLAB_COM_GROUP, described_class::GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION
      end

      describe '.gitlab_core_team_community_members_group_member_ids' do
        it_behaves_like 'cached group member attributes', :gitlab_core_team_community_members_group_member_ids, :id, described_class::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP, described_class::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION
      end

      describe '.gitlab_org_group_member_usernames' do
        it_behaves_like 'cached group member attributes', :gitlab_org_group_member_usernames, :username, described_class::GITLAB_ORG_GROUP, described_class::GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION
      end

      describe '.gitlab_com_group_member_usernames' do
        it_behaves_like 'cached group member attributes', :gitlab_com_group_member_usernames, :username, described_class::GITLAB_COM_GROUP, described_class::GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION
      end

      describe '.gitlab_core_team_community_members_group_member_usernames' do
        it_behaves_like 'cached group member attributes', :gitlab_core_team_community_members_group_member_usernames, :username, described_class::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP, described_class::GITLAB_CORE_TEAM_COMMUNITY_MEMBERS_GROUP_MEMBERS_CACHE_EXPIRATION
      end
    end
  end
end
